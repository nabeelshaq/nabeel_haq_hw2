package com.hw2.animal;

import com.hw2.processor.AbstractFactoryType;

@AbstractFactoryType(
        id = "Cat",
        type = AnimalFactory.class
)
public class CatFactory implements AnimalFactory {
    @Override
    public AnimalFactory create() {
        return (AnimalFactory) new CatBark();
    }

}