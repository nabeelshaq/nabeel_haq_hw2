package com.hw2.animal;

public interface AnimalFactory<T> {
    T create();
}
