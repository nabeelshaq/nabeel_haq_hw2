package com.hw2.animal;

import com.hw2.processor.AbstractFactoryType;

@AbstractFactoryType(
        id = "CatBark",
        type = Cat.class
)
public class CatBark implements Cat {
    public String bark() {
        return "meow";
    }
}
