package com.hw2.animal;

import com.hw2.processor.AbstractFactoryType;

@AbstractFactoryType(
        id = "DogBark",
        type = Dog.class
)
public class DogBark implements Dog {
    public String bark() {
        return "bark";
    }
}
