package com.hw2;

import com.hw2.animal.CatFactory;
import com.hw2.animal.DogFactory;
import com.hw2.food.CalzoneFactory;
import com.hw2.food.PizzaFactory;
import com.hw2.kingdom.ElfKingdomFactory;
import com.hw2.kingdom.OrcKingdomFactory;
import com.hw2.processor.AbstractFactoryProcessor;


// this class instantiates all of the example abstract factories and runs the annotation processor on them
public class App {

    public static void main(String[] args) {
        Class orc = OrcKingdomFactory.class;
        Class elf = ElfKingdomFactory.class;
        Class calzone = CalzoneFactory.class;
        Class pizza = PizzaFactory.class;
        Class dog = DogFactory.class;
        Class cat = CatFactory.class;
        AbstractFactoryProcessor a = new AbstractFactoryProcessor();
        a.processing(pizza);
        a.processing(calzone);
        a.processing(orc);
        a.processing(elf);
        a.processing(dog);
        a.processing(cat);

    }
}