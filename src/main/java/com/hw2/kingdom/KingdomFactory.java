package com.hw2.kingdom;
// i found this implementation on github from user iluwatar
public interface KingdomFactory {
	Castle createCastle();
	King createKing();
	Army createArmy();
}
