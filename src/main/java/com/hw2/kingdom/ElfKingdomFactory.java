package com.hw2.kingdom;

import com.hw2.processor.AbstractFactoryType;

@AbstractFactoryType(
		id = "ELF",
		type = KingdomFactory.class
)

public class ElfKingdomFactory implements KingdomFactory {
	public Castle createCastle() {
		return new ElfCastle();
	}
	public King createKing() {
		return new ElfKing();
	}
	public Army createArmy() {
		return new ElfArmy();
	}
}
