package com.hw2.kingdom;

import com.hw2.processor.AbstractFactoryType;

@AbstractFactoryType(
		id = "ORC",
		type = KingdomFactory.class
)

public class OrcKingdomFactory implements KingdomFactory {
	public Castle createCastle() {
		return new OrcCastle();
	}
	public King createKing() {
		return new OrcKing();
	}
	public Army createArmy() {
		return new OrcArmy();
	}
}
