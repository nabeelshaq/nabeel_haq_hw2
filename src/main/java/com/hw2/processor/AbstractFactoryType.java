package com.hw2.processor;

import java.lang.annotation.*;
// created annotation
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AbstractFactoryType {
    String id();
    Class type();
}