package com.hw2.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.annotation.Annotation;
import java.lang.Object;
import java.lang.reflect.Modifier;


public class AbstractFactoryProcessor {
    private static final Logger logger = LoggerFactory.getLogger(AbstractFactoryProcessor.class);
    public void processing(Class o) {
        // create an empty array to test with getInterfaces call
        Object[] no_interfaces = new testClass[0];
        // create empty array to test with getConstructors
        Object[] no_constructors = new testClass[0];
        // boolean will return true after passing all checks
        boolean check = false;
        Class obj = o;
        logger.info(obj.getCanonicalName());
        // process the @AbstractFactoryType annotations
        if (obj.isAnnotationPresent(AbstractFactoryType.class)) {
            // check if the class is implementing interfaces
            if (obj.getInterfaces()==no_interfaces){
                logger.warn("FAILED INTERFACE CHECK");
            }
            else{
                logger.info("Class is successfully implementing interface.");
                // check if the class has a constructor
                if(obj.getConstructors()==no_constructors){
                    logger.warn("FAILED CONSTRUCTOR CHECK");
                }
                else{
                    logger.info("Class has a constructor.");
                    // get class modifiers to check if class is public or abstract
                    int modifierCheck = obj.getModifiers();
                    String strModifier = Modifier.toString(modifierCheck);
                    if (strModifier.contains("public")){
                        logger.info("Class is public");
                        check = true;
                    }
                    else if (strModifier.contains("abstract")){
                        logger.warn("FAILED ABSTRACT CLASS TEST");
                    }

                }
            }
            // pass all checks
            if (check == true){
                logger.info("Successfully implemented Abstract Factory Design Pattern!");
            }
            // failed a check
            else{
                logger.warn("Incorrect implementation of Abstract Factory Design Pattern");
            }

        }
        // no @AbstractFactoryType annotations found
        else{
            logger.warn("AbstractFactoryType annotation does not exist");
        }

        
    }
}