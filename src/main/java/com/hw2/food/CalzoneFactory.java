package com.hw2.food;

import com.hw2.processor.AbstractFactoryType;

@AbstractFactoryType(
        id = "Calzone",
        type = AbstractFactory.class
)

public class CalzoneFactory implements AbstractFactory {
    @Override
    public AbstractFactory createCheese() {
        return (AbstractFactory) new CalzoneCheese();
    }

    @Override
    public AbstractFactory createSauce() {
        return (AbstractFactory) new CalzoneSauce();
    }
}