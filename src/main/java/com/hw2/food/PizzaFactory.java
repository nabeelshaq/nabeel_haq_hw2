package com.hw2.food;

import com.hw2.processor.AbstractFactoryType;

@AbstractFactoryType(
        id = "Pizza",
        type = AbstractFactory.class
)
public class PizzaFactory implements AbstractFactory {
    @Override
    public AbstractFactory createCheese() {
        return (AbstractFactory) new PizzaCheese();
    }

    @Override
    public AbstractFactory createSauce() {
        return (AbstractFactory) new PizzaSauce();
    }
}