package com.hw2.food;

import com.hw2.processor.AbstractFactoryType;

@AbstractFactoryType(
        id = "CalzoneCheese",
        type = Calzone.class
)
public class CalzoneCheese implements Calzone {
    public float getPrice() {
        return 3f;
    }
}
