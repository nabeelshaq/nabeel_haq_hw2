package com.hw2.food;

public interface AbstractFactory<T> {
    T createSauce();
    T createCheese();
}
