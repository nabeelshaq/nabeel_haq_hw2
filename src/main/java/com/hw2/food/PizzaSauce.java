package com.hw2.food;

import com.hw2.processor.AbstractFactoryType;


@AbstractFactoryType(
    id = "PizzaSauce",
    type = Pizza.class
)
public class PizzaSauce implements Pizza {
  public float getPrice() {
    return 6f;
  }
}
