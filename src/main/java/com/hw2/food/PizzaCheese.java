package com.hw2.food;

import com.hw2.processor.AbstractFactoryType;

@AbstractFactoryType(
        id = "PizzaCheese",
        type = Pizza.class
)
public class PizzaCheese implements Pizza {
    public float getPrice() {
        return 4f;
    }
}
