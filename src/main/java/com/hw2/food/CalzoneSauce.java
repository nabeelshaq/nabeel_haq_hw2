package com.hw2.food;

import com.hw2.processor.AbstractFactoryType;


@AbstractFactoryType(
        id = "CalzoneSauce",
        type = Pizza.class
)
public class CalzoneSauce implements Calzone {
    public float getPrice() {
        return 6f;
    }
}
