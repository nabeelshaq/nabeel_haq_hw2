package test;

import com.hw2.animal.AnimalFactory;
import com.hw2.animal.CatFactory;
import com.hw2.animal.DogFactory;
import com.hw2.food.AbstractFactory;
import com.hw2.food.CalzoneFactory;
import com.hw2.food.PizzaFactory;
import com.hw2.kingdom.ElfKingdomFactory;
import com.hw2.kingdom.KingdomFactory;
import com.hw2.kingdom.OrcKingdomFactory;
import com.hw2.processor.AbstractFactoryType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;


import java.lang.annotation.Annotation;

import static org.junit.Assert.*;

public class AppTest{
    private static Logger logger = LogManager.getLogger();
    // add logger messages for junit
    @Rule
    public TestWatcher watchman = new TestWatcher() {

        @Override
        protected void failed(Throwable e, Description description) {
            logger.error(description, e);
        }

        @Override
        protected void succeeded(Description description) {
            logger.info(description);
        }
    };
    // tests orckingdomfactory
    @Test
    public void testOrcKingdom(){
        Class orc = OrcKingdomFactory.class;
        Annotation annotation = orc.getAnnotation(AbstractFactoryType.class);
        if (annotation instanceof AbstractFactoryType){
            AbstractFactoryType abs = (AbstractFactoryType) annotation;
            assertEquals("ORC", abs.id());
            assertEquals(KingdomFactory.class,abs.type());
        }
        else{
            fail();
        }
    }
    // tests elfkingdomfactory
    @Test
    public void testElfKingdom(){
        Class elf = ElfKingdomFactory.class;
        Annotation annotation = elf.getAnnotation(AbstractFactoryType.class);
        if (annotation instanceof AbstractFactoryType){
            AbstractFactoryType abs = (AbstractFactoryType) annotation;
            assertEquals("ELF", abs.id());
            assertEquals(KingdomFactory.class,abs.type());
        }
        else{
            fail();
        }
    }
    // tests calzonefactory
    @Test
    public void testCalzone(){
        Class calzone = CalzoneFactory.class;
        Annotation annotation = calzone.getAnnotation(AbstractFactoryType.class);
        if (annotation instanceof AbstractFactoryType){
            AbstractFactoryType abs = (AbstractFactoryType) annotation;
            assertEquals("Calzone", abs.id());
            assertEquals(AbstractFactory.class,abs.type());
        }
        else{
            fail();
        }
    }
    // tests pizzafactory
    @Test
    public void testPizza(){
        Class pizza = PizzaFactory.class;
        Annotation annotation = pizza.getAnnotation(AbstractFactoryType.class);
        if (annotation instanceof AbstractFactoryType){
            AbstractFactoryType abs = (AbstractFactoryType) annotation;
            assertEquals("Pizza", abs.id());
            assertEquals(AbstractFactory.class,abs.type());
        }
        else{
            fail();
        }
    }
    // tests dogfactory
    @Test
    public void testDog(){
        Class dog = DogFactory.class;
        Annotation annotation = dog.getAnnotation(AbstractFactoryType.class);
        if (annotation instanceof AbstractFactoryType){
            AbstractFactoryType abs = (AbstractFactoryType) annotation;
            assertEquals("Dog", abs.id());
            assertEquals(AnimalFactory.class,abs.type());
        }
        else{
            fail();
        }
    }
    // tests catfactory
    @Test
    public void testCat(){
        Class cat = CatFactory.class;
        Annotation annotation = cat.getAnnotation(AbstractFactoryType.class);
        if (annotation instanceof AbstractFactoryType){
            AbstractFactoryType abs = (AbstractFactoryType) annotation;
            assertEquals("Cat", abs.id());
            assertEquals(AnimalFactory.class,abs.type());
        }
        else{
            fail();
        }
    }


}