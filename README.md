Nabeel Haq - CS 474 - Homework 2

DESCRIPTION:

My assigned design pattern was the Abstract Factory Design Pattern.
The Abstract Factory pattern provides a way to encapsulate a group of individual factories that have a common theme
without specifying their concrete classes. I found a few implementations online and I used one of them, the kingdom factory, to base the rest of my implementations off of.
To create my annotation processor, I followed a video guide that was posted on piazza by dr. mark.
https://www.youtube.com/watch?v=J2GohD6r8Co&t=23s
I built my annotation using the baseline example from the official java annotations guide.
https://docs.oracle.com/javase/1.5.0/docs/guide/language/annotations.html
To check if the implementation of an abstract factory design was done correctly, my processor checks for a few things:
1. If the class that is annotated is public, which would succeed
2. If the class that is annotated is abstract, which would fail
3. If the class has a constructor, which would succeed
4. If the class is implementing an interface, which would succeed
If it doesn't pass these basic checks, then the processor would deem it an incorrect implementation of the Abstract Factory Design.
My annotation is @AbstractFactoryType, and checks for a String called id and a Class called type.
To do these checks, my code first checks if the annotation is present in the give class with .isAnnotationPresent
To check if there is an interface it uses .getInterfaces(), to check if it has a constructor it uses .getConstructors, and to check if it is public or abstract, it uses .getModifiers()
In my junit tests, it uses the java lang annotation instances to check if the id and the type of the annotation is what they are expected to be.
The annotation process is located in src/main/java/com.hw2.processor
The examples are com.hw2.animal, com.hw2.food, and com.hw2.kingdom
The junit tests are located in the path src/main/java/test
There is also a class called App.java in com.hw2 which instantiates all the examples and processes the annotations on all of them so that when the program is run, it will immediately check the examples that I provided.

TO RUN:

This program was created with Java and using Gradle, so to run the program, you just need to enter the commands ./gradlew clean run in the terminal and it will compile. 
When the program is run, it will log the messages that I created telling you if it passes all the checks and if the design pattern is implemented correctly. If it is 
implemented incorrectly, it will warn you telling you what you need to fix based on the checks that the processor does.
